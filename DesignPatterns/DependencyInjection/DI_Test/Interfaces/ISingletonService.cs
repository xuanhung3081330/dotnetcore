﻿using System;

namespace DI_Test.Interfaces
{
    public interface ISingletonService
    {
        Guid GetID();
    }
}
