﻿using System;

namespace DI_Test.Interfaces
{
    public interface ITransientService
    {
        Guid GetID();
    }
}
