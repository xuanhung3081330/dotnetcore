﻿using System;

namespace DI_Test.Interfaces
{
    public class SomeService : ITransientService, IScopedService, ISingletonService
    {
        Guid id;

        public SomeService()
        {
            id = Guid.NewGuid();
        }

        public Guid GetID()
        {
            return id;
        }
    }
}
