﻿using System;

namespace DI_Test.Interfaces
{
    public interface IScopedService
    {
        Guid GetID();
    }
}
