﻿using DI_Test.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DI_Test.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly ITransientService _transientService1;
        private readonly ITransientService _transientService2;

        private readonly IScopedService _scopedService1;
        private readonly IScopedService _scopedService2;

        private readonly ISingletonService _singletonService1;
        private readonly ISingletonService _singletonService2;

        public HomeController(
            ITransientService transientService1,
            ITransientService transientService2,
            IScopedService scopedService1,
            IScopedService scopedService2,
            ISingletonService singletonService1,
            ISingletonService singletonService2)
        {
            _transientService1 = transientService1;
            _transientService2 = transientService2;

            _scopedService1 = scopedService1;
            _scopedService2 = scopedService2;

            _singletonService1 = singletonService1;
            _singletonService2 = singletonService2;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(new
            {
                FirstTransient = _transientService1.GetID().ToString(),
                SecondTransient = _transientService2.GetID().ToString(),
                FirstScoped = _scopedService1.GetID().ToString(),
                SecondScoped = _scopedService2.GetID().ToString(),
                FirstSingleton = _singletonService1.GetID().ToString(),
                SecondSingleton = _singletonService2.GetID().ToString()
            });
        }
    }
}
