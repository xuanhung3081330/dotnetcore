﻿namespace OptionsTest.Options
{
    public class DraftOption
    {
        public const string Draft = "Draft";

        public int Number { get; set; }
        public string Text { get; set; }
    }
}
