﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using OptionsTest.Options;
using System.Net.Http;

namespace OptionsTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly DraftOption _options;

        // Option 1: Use Configure method in DI Service Container
        public HomeController(IOptions<DraftOption> options)
        {
            _options = options.Value;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_options);
        }
    }
}
