﻿using System;
using System.Linq;

namespace YieldTest
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Using List
            var storeLst = ContactListStore.GetEnumerator();

            // Using Yield
            var storeYield = ContactYieldStore.GetEnumerator();

            Console.WriteLine("Ready to iterate through the collection");
            //Console.WriteLine("Hello {0}", storeYield.First().FirstName);
            Console.ReadKey();
        }
    }
}
