﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YieldTest
{
    public static class ContactYieldStore
    {
        public static IEnumerable<Contact> GetEnumerator()
        {
            Console.WriteLine("ContactYieldStore: Creating contact 1");
            yield return new Contact
            {
                FirstName = "Bob",
                LastName = "Blue"
            };

            Console.WriteLine("ContactYieldStore: Creating contact 2");
            yield return new Contact
            {
                FirstName = "Jim",
                LastName = "Green"
            };

            Console.WriteLine("ContactYieldStore: Creating contact 3");
            yield return new Contact
            {
                FirstName = "Susan",
                LastName = "Orange"
            };
        }
    }
}
