﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YieldTest
{
    public static class ContactListStore
    {
        public static IEnumerable<Contact> GetEnumerator()
        {
            var contacts = new List<Contact>();
            Console.WriteLine("ContactListStore: Creating contact 1");
            contacts.Add(new Contact
            {
                FirstName = "Bob",
                LastName = "Blue"
            });

            Console.WriteLine("ContactListStore: Creating contact 2");
            contacts.Add(new Contact
            {
                FirstName = "Jim",
                LastName = "Green"
            });

            Console.WriteLine("ContactListStore: Creating contact 3");
            contacts.Add(new Contact
            {
                FirstName = "Susan",
                LastName = "Orange"
            });

            return contacts;
        }
    }
}
