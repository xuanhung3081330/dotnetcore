﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwaitTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Main Thread: {Thread.CurrentThread.ManagedThreadId} Started");
            // Option 1: Using Task class and Start
            //Task task1 = new Task(PrintCounter);
            //task1.Start();

            // Option 2: Creating a Task object using the Run method.
            //var task1 = Task.Run(() =>
            //{
            //    PrintCounter();
            //});

            // Option 3: Wait for the task to complete
            //var task1 = Task.Run(() =>
            //{
            //    PrintCounter();
            //});
            //task1.Wait();

            // Option 4: Using generic Task<T>
            //Task<double> task1 = Task.Run(() =>
            //{
            //    return CalculateSum(10);
            //});
            //Console.WriteLine($"Sum is: {task1.Result}");

            // Option 5: Returning complex type value from a Task
            Task<Employee> task = Task.Run(() =>
            {
                var employee = new Employee
                {
                    Id = 1,
                    Name = "Hung",
                    Salary = 10000
                };

                return employee;
            });

            var emp = task.Result;
            Console.WriteLine($"ID: {emp.Id}, Name: {emp.Name}, Salary: {emp.Salary}");
            
            Console.WriteLine($"Main Thread: {Thread.CurrentThread.ManagedThreadId} Completed");
            Console.ReadKey();
        }

        //static double CalculateSum(int num)
        //{
        //    double sum = 0;
        //    for (var i = 1; i <= num; i++)
        //    {
        //        sum += i;
        //    }

        //    return sum;
        //}

        //static void PrintCounter()
        //{
        //    Console.WriteLine($"Child Thread: {Thread.CurrentThread.ManagedThreadId} Started");
        //    for (var i = 1; i <= 5; i++)
        //    {
        //        Console.WriteLine($"i value: {i}");
        //    }
        //    Console.WriteLine($"Child Thread: {Thread.CurrentThread.ManagedThreadId} Completed");
        //}
    }

    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Salary { get; set; }
    }
}
