﻿using HtmlAgilityPack;

namespace GitFun.Services
{
    public static class ScrapePage
    {
        public static HtmlDocument GetDocument(string url)
        {
            var web = new HtmlWeb();
            var doc = web.Load(url);
            return doc;
        }

        public static HtmlNode CustomSelectSingleNode(string url, string tagToFind)
        {
            var web = new HtmlWeb();
            var doc = web.Load(url);
            var node = doc.DocumentNode.SelectSingleNode(tagToFind);
            return node;
        }

        public static HtmlNode CustomSelectMultiNodes(string url, string tagToFind)
        {
            var web = new HtmlWeb();
            var doc = web.Load(url);
            var node = doc.DocumentNode.SelectSingleNode(tagToFind);
            return node;
        }
    }
}
