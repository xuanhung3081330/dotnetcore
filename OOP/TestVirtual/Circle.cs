﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestVirtual
{
    public class Circle : Polynomial
    {
        public double Radius { get; set; }

        public override double Area()
        {
            return Math.PI * Radius * Radius;
        }
    }
}
