﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestVirtual
{
    public class Triangle : Polynomial
    {
        public override double Area()
        {
            return 0.5 * Len * Width;
        }
    }
}
