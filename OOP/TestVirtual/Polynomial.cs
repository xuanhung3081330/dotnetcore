﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestVirtual
{
    public class Polynomial
    {
        public virtual double Len { get; set; }
        public virtual double Width { get; set; }

        public virtual double Area()
        {
            return 0;
        }
    }
}
