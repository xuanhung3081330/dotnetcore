﻿using System;

namespace TestVirtual
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Rectangle
            var rect = new Rectangle();
            rect.Width = 10;
            rect.Len = 5;
            Console.WriteLine("Area of Rectangle = " + rect.Area());

            // Square
            var sq = new Square();
            sq.Len = 15;
            Console.WriteLine("Area of Square = " + sq.Area());

            // Circle
            //var circle = new Circle();
            //circle.Radius = 10;
            //Console.WriteLine("Area of Circle = " + circle.Area());

            // Triangle
            //var triangle = new Triangle();
            //triangle.Len = 5;
            //triangle.Width = 10;
            //Console.WriteLine("Area of Triangle = " + triangle.Area());

            Console.ReadKey();
        }
    }
}
