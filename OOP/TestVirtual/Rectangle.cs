﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestVirtual
{
    public class Rectangle : Polynomial
    {
        public override double Area()
        {
            return Len * Width;
        }
    }
}
