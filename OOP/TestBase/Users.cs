﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestBase
{
    public class Users
    {
        public string name = "Suresh Dasari";
        public string location = "Hyderabad";
        public int age = 32;

        public virtual void GetInfo()
        {
            Console.WriteLine("Name: {0}", name);
            Console.WriteLine("Location: {0}", location);
        }
    }
}
