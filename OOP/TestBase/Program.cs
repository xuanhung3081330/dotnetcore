﻿using System;

namespace TestBase
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var details = new Details();
            details.GetInfo();
            Console.ReadKey();
        }
    }
}
