﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOPDemo
{
    public class ExpeditedPackage : Package
    {
        public ExpeditedPackage(
            string recipient, 
            string address, 
            decimal weight, DateTime sendDate) : base(recipient, address, weight, sendDate)
        {
        }

        public sealed override decimal GetDeliveryCost()
        {
            return 4 * Weight + 2;
        }

        public sealed override DateTime GetDeliveryDate()
        {
            return SendDate.AddDays(1);
        }
    }
}
