﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOPDemo
{
    public class Package
    {
        public string Recipient { get; set; }
        public string Address { get; set; }
        public decimal Weight { get; set; }
        public DateTime SendDate { get; set; }

        public Package(string recipient, string address, decimal weight, DateTime sendDate)
        {
            Recipient = recipient;
            Address = address;
            Weight = weight;
            SendDate = sendDate;
        }

        public virtual decimal GetDeliveryCost()
        {
            return 3 * Weight;
        }

        public virtual DateTime GetDeliveryDate()
        {
            if (SendDate.DayOfWeek == DayOfWeek.Friday)
                return SendDate.AddDays(4);
            else if (SendDate.DayOfWeek == DayOfWeek.Thursday)
                return SendDate.AddDays(4);
            else
                return SendDate.AddDays(2);
        }
    }
}
