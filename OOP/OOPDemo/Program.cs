﻿using System;

namespace OOPDemo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Encapsulation
            var sbi = new Bank();
            sbi.SetBalance(100);
            Console.WriteLine(sbi.GetBalance());

            // Abstraction
            var cat = new Cat();
            Console.WriteLine(cat.Roar());
            var dog = new Dog();
            Console.WriteLine(dog.Roar());

            // Polymorphism
            var expeditedPackage = new ExpeditedPackage(
                "Uyen",
                "Danang",
                12.4M,
                DateTime.Now);
            Console.WriteLine($"Expedited package delivery cost: {expeditedPackage.GetDeliveryCost()}");
            Console.WriteLine($"Expedited package delivery date: {expeditedPackage.GetDeliveryDate()}");

            var internationalPackage = new InternationalPackage(
                "Hung",
                "TPHCM",
                8.4M,
                DateTime.Now.AddDays(3));
            Console.WriteLine($"International package delivery cost: {internationalPackage.GetDeliveryCost()}");
            Console.WriteLine($"International package delivery date: {internationalPackage.GetDeliveryDate()}");

            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }
    }
}
