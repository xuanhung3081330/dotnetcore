﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOPDemo
{
    public abstract class Animal
    {
        public abstract string Roar();
    }
}
