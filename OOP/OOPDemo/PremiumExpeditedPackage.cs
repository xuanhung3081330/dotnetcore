﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOPDemo
{
    public class PremiumExpeditedPackage : ExpeditedPackage
    {
        public PremiumExpeditedPackage(
            string recipient, 
            string address, 
            decimal weight, 
            DateTime sendDate) : base(recipient, address, weight, sendDate)
        {
        }

        // Cannot override the methods in ExpeditedPackage
    }
}
