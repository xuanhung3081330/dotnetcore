﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOPDemo
{
    public class InternationalPackage : Package
    {
        public InternationalPackage(
            string recipient, 
            string address, 
            decimal weight, 
            DateTime sendDate) : base(recipient, address, weight, sendDate)
        {
        }

        public string CountryCode { get; set; }

        public override decimal GetDeliveryCost()
        {
            return 4 * Weight;
        }

        public override DateTime GetDeliveryDate()
        {
            return SendDate.AddDays(5);
        }
    }
}
