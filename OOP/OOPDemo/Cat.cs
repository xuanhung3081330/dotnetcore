﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOPDemo
{
    public class Cat : Animal
    {
        public override string Roar()
        {
            return "Meow meow";
        }
    }
}
