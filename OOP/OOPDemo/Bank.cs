﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOPDemo
{
    public class Bank
    {
        // Hiding class data by declaring the variable as private
        private double balance;

        // Creating public setter and getter methods
        public double GetBalance()
        {
            return balance;
        }

        public void SetBalance(double balance)
        {
            // Add validation logic to check whether data is correct or not
            // We dont allow users to access directly the "balance" variable
            this.balance = balance;
        }
    }
}
