﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOPDemo
{
    public class Dog : Animal
    {
        public override string Roar()
        {
            return "Gaow gaow";
        }
    }
}
