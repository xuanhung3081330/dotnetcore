﻿using System;
using System.IO;

namespace TestHandleException
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // 1: Use custom exception
            //try
            //{
            //    throw new UserAlreadyLoggedInException("User Already Logged in!!!");
            //}
            //catch (UserAlreadyLoggedInException ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}

            // 2: Use InnerException
            try
            {
                try
                {
                    Console.WriteLine("Enter the first number: ");
                    var fn = Convert.ToInt32(Console.ReadLine());

                    Console.WriteLine("Enter the second number: ");
                    var sn = Convert.ToInt32(Console.ReadLine());

                    var result = fn / sn;
                    Console.WriteLine("Result = {0}", result);
                }
                catch (Exception ex)
                {
                    var filePath = @"C:\logfile\Log.txt";
                    if (File.Exists(filePath))
                    {
                        StreamWriter sw = new StreamWriter(filePath);
                        sw.Write(ex.GetType().Name + ex.Message + ex.StackTrace);
                        sw.Close();
                        Console.WriteLine("There is a problem! Plese try later");
                    }
                    else
                    {
                        //To retain the original exception pass it as a parameter
                        //to the constructor, of the current exception
                        throw new FileNotFoundException(filePath + " Does not Exist", ex);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Current or Outer Exception = " + ex.Message);

                if (ex.InnerException != null)
                {
                    Console.Write("Inner Exception: ");
                    Console.WriteLine(String.Concat(ex.InnerException.StackTrace, ex.InnerException.Message));
                }
            }
            Console.WriteLine("End of the program");
            Console.ReadKey();
        }
    }
}
