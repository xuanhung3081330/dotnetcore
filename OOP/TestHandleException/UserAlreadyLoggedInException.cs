﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestHandleException
{
    public class UserAlreadyLoggedInException : Exception
    {
        public UserAlreadyLoggedInException(string message) 
            : base(message)
        {
        }

        public UserAlreadyLoggedInException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
