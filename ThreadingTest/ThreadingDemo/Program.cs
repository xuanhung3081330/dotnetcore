﻿using System;
using System.Threading;

namespace ThreadingDemo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Main thread starts here.");

            // Create a second thread
            //Thread backgroundThread = new Thread(new ThreadStart(DoSomeHeavyLifting));

            // Start second thread
            //backgroundThread.Start();

            //DoSomething();

            //Thread workerThread = new Thread(new ThreadStart(Print));
            //workerThread.Start();

            //for (var i = 0; i < 10; i++)
            //{
            //    Console.WriteLine($"Main thread: {i}");
            //    Thread.Sleep(200);
            //}

            //Console.WriteLine("Main thread ends here.");

            // Use ThreadPool for a worker thread
            var person = new Person("Hung", 24, "Male");
            ThreadPool.QueueUserWorkItem(BackgroundTaskWithObject, person);
            Console.WriteLine("Main thread running");
            Thread.Sleep(1000);
            var workers = 0;
            var ports = 0;
            ThreadPool.GetMaxThreads(out workers, out ports);
            Console.WriteLine(workers);
            Console.WriteLine(ports);

            Console.ReadKey();
        }

        //private static void Print()
        //{
        //    for (var i = 11; i < 20; i++)
        //    {
        //        Console.WriteLine($"Worker thread: {i}");
        //        Thread.Sleep(1000);
        //    }
        //}

        //private static void DoSomeHeavyLifting()
        //{
        //    Console.WriteLine("I'm lifting a truck!!!");
        //    Thread.Sleep(1000);
        //    Console.WriteLine("Tired! Need a 3 sec nap");
        //    Thread.Sleep(1000);
        //    Console.WriteLine("1...");
        //    Thread.Sleep(1000);
        //    Console.WriteLine("2...");
        //    Thread.Sleep(1000);
        //    Console.WriteLine("3...");
        //    Console.WriteLine("Awake!!!");
        //}

        //private static void DoSomething()
        //{
        //    Console.WriteLine("Do something here!!!");
        //    for (var i = 0; i < 20; i++)
        //    {
        //        Console.Write($"{i}");
        //    }

        //    Console.WriteLine();
        //    Console.WriteLine("I'm done");
        //}

        private static void BackgroundTaskWithObject(Object stateInfo)
        {
            var person = stateInfo as Person;
            Console.WriteLine($"Hi {person.Name} from ThreadPool");
            Thread.Sleep(1000);
        }
    }
}
