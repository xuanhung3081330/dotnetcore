﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ThreadingDemo
{
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Sex { get; set; }

        public Person(string name, int age, string sex)
        {
            Name = name;
            Age = age;
            Sex = sex;
        }
    }
}
