﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace RoutingTest.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        [HttpGet]
        public IActionResult Get()
        {
            return Ok("This is home api");
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            return Ok($"This is home api for getting id {id}");
        }
    }
}
