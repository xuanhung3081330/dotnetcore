﻿using Facebook.Post.Infrastructure.ModelConfigurations;
using Microsoft.EntityFrameworkCore;

namespace Facebook.Post.Infrastructure.Contexts
{
    public class PostContext : DbContext
    {
        public DbSet<Facebook.Post.Domain.Post> Posts { get; set; }

        public PostContext(DbContextOptions<PostContext> options)
            : base(options)
        {
        }

        //This method should be used only in when we add migration and update databaes(design time)
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer("Server=localhost;Database=FacebookDB;Integrated Security=true");
        //}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new PostEntityTypeConfiguration());
        }
    }
}
