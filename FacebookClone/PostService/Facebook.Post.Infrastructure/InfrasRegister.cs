﻿using Facebook.Post.Domain;
using Facebook.Post.Infrastructure.Implementations;
using Microsoft.Extensions.DependencyInjection;

namespace Facebook.Post.Infrastructure
{
    public static class InfrasRegister
    {
        public static IServiceCollection AddRepos(IServiceCollection services)
        {
            services.AddTransient<IPostRepository, PostRepository>();

            return services;
        }
    }
}
