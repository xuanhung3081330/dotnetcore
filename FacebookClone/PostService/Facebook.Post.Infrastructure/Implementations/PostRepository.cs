﻿using Facebook.Post.Domain;
using Facebook.Post.Infrastructure.Contexts;
using System;
using System.Threading.Tasks;

namespace Facebook.Post.Infrastructure.Implementations
{
    public class PostRepository : IPostRepository
    {
        private readonly PostContext _context;

        public PostRepository(PostContext context)
        {
            _context = context;
        }

        public async Task CreatePostAsync(Domain.Post post)
        {
            await _context.Posts.AddAsync(post);
            await _context.SaveChangesAsync();
        }
    }
}
