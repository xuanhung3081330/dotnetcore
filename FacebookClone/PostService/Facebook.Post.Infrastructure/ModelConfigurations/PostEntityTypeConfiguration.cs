﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Facebook.Post.Infrastructure.ModelConfigurations
{
    public class PostEntityTypeConfiguration : IEntityTypeConfiguration<Facebook.Post.Domain.Post>
    {
        public void Configure(EntityTypeBuilder<Domain.Post> builder)
        {
            builder
                .HasKey(x => x.Id);

            builder
                .Property(x => x.Content)
                .IsRequired();

            builder
                .Property(x => x.DeletedAt)
                .HasDefaultValue(null);

            builder
                .Property(x => x.IsDeleted)
                .HasDefaultValue(false);

            //SeedData(builder);
        }

        // To seed data only
        //private void SeedData(EntityTypeBuilder<Domain.Post> builder)
        //{
        //    builder.HasData
        //    (
        //        new Domain.Post("Draft content 1"),
        //        new Domain.Post("Draft content 2"),
        //        new Domain.Post("Draft content 3")
        //    );
        //}
    }
}
