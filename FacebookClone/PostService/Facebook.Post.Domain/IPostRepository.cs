﻿using System.Threading.Tasks;

namespace Facebook.Post.Domain
{
    public interface IPostRepository
    {
        Task CreatePostAsync(Post post);
    }
}
