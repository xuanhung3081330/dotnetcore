﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Facebook.Post.Domain
{
    public class Post : IValidatableObject
    {
        public Guid Id { get; }
        public string Content { get; }
        public int Likes { get; } = 0;
        public DateTime CreatedAt { get; private set; } = DateTime.Now;
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedAt { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(Content))
            {
                yield return new ValidationResult("Content cannot be null or empty!!!");
            }
        }
    }
}
