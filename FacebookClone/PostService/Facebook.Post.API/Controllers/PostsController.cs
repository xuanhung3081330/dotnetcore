﻿using AutoMapper;
using Facebook.Post.Domain;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Facebook.Post.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly IPostRepository _postRepo;
        private readonly IMapper _mapper;

        public PostsController(IPostRepository postRepo, IMapper mapper)
        {
            _postRepo = postRepo;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> CreatePostAsync([FromBody] Facebook.Post.API.ViewModels.Post post)
        {
            try
            {
                var mappedPost = _mapper.Map<Facebook.Post.Domain.Post>(post);
                await _postRepo.CreatePostAsync(mappedPost);
                return Ok(new HttpResponseMessage(System.Net.HttpStatusCode.Created));
            }
            catch (ArgumentNullException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
