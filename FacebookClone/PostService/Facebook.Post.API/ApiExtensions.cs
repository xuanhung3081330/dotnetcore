﻿using AutoMapper;
using Facebook.Post.API.MappingProfiles;
using Microsoft.Extensions.DependencyInjection;

namespace Facebook.Post.API
{
    public static class ApiExtensions
    {
        public static IServiceCollection AddAutoMapper(this IServiceCollection services)
        {
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new PostProfile());
            });

            var mapper = mapperConfig.CreateMapper();
            services.AddSingleton<IMapper>(mapper);

            return services;
        }
    }
}
