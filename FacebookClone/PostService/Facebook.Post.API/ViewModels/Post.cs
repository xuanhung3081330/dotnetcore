﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Facebook.Post.API.ViewModels
{
    public class Post : IValidatableObject
    {
        public string Content { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(Content))
            {
                yield return new ValidationResult("Content cannot be null or empty");
            }
        }
    }
}
