﻿using AutoMapper;

namespace Facebook.Post.API.MappingProfiles
{
    public class PostProfile : Profile
    {
        public PostProfile()
        {
            CreateMap<Facebook.Post.API.ViewModels.Post, Facebook.Post.Domain.Post>()
                .ForMember(dest => dest.Content, opt => opt.MapFrom(src => src.Content));
        }
    }
}
