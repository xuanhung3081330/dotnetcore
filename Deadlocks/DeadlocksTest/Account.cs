﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeadlocksTest
{
    public class Account
    {
        public int Id { get; }
        private double Balance;

        public Account(int id, double balance)
        {
            Id = id;
            Balance = balance;
        }

        public void WithdrawMoney(double amount)
        {
            Balance -= amount;
        }

        public void DepositMoney(double amount)
        {
            Balance += amount;
        }
    }
}
